import React from 'react';
import PropTypes from 'prop-types';
import {
  Router, Switch, Route
} from 'dva/router';
import Dynamic from 'dva/dynamic';
import gd_page1 from "./routes/gd_page1";

function RouterConfig({
  history, app
}) {
  const Index = Dynamic({
    app,
    models: () => [
      import('./models/example')
    ],
    component: () => import('./routes/index')
  });
  const Page01 = Dynamic({
    app,
    component: () => import('./routes/page01')
  });
  const Page02 = Dynamic({
    app,
    component: () => import('./routes/page02')
  });
  const Page03 = Dynamic({
    app,
    component: () => import('./routes/page03')
  });

  const Xxe = Dynamic({
    app,
    models: () => [
      import('./models/example')
    ],
    component: () => import('./routes/xxe')
  });
  const Gd = Dynamic({
    app,
    models: () => [
      import('./models/example')
    ],
    component: () => import('./routes/gd')
  });
  const Gd_cj = Dynamic({
    app,
    models: () => [
      import('./models/example')
    ],
    component: () => import('./routes/gd_cj')
  });
  const Gd_page1 = Dynamic({
    app,
    component: () => import('./routes/gd_page1')
  });
  const Gd_page2 = Dynamic({
    app,
    component: () => import('./routes/gd_page2')
  });
  const Gd_page3 = Dynamic({
    app,
    component: () => import('./routes/gd_page3')
  });
  const Xxe_page1 = Dynamic({
    app,
    component: () => import('./routes/xxe_page1')
  });
  const Xxe_page2 = Dynamic({
    app,
    component: () => import('./routes/xxe_page2')
  });
  const Xxe_page3 = Dynamic({
    app,
    component: () => import('./routes/xxe_page3')
  });
  const Xx_page1 = Dynamic({
    app,
    component: () => import('./routes/xx_page1')
  });
  const Xx_page2 = Dynamic({
    app,
    component: () => import('./routes/xx_page2')
  });

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Index} />
        <Route exact path="/page01" component={Page01} />
        <Route exact path="/page02" component={Page02} />
        <Route exact path="/page03" component={Page03} />
        <Route exact path="/xxe" component={Xxe} />
        <Route exact path="/gd" component={Gd} />
        <Route exact path="/gd_cj" component={Gd_cj} />
        <Route exact path="/gd_page1" component={Gd_page1} />
        <Route exact path="/gd_page2" component={Gd_page2} />
        <Route exact path="/gd_page3" component={Gd_page3} />
        <Route exact path="/xxe_page1" component={Xxe_page1} />
        <Route exact path="/xxe_page2" component={Xxe_page2} />
        <Route exact path="/xxe_page3" component={Xxe_page3} />
        <Route exact path="/xx_page1" component={Xx_page1} />
        <Route exact path="/xx_page2" component={Xx_page2} />
      </Switch>
    </Router>
  );
}

RouterConfig.propTypes = {
  history: PropTypes.object.isRequired
};

export default RouterConfig;
