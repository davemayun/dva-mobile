
export default {

  namespace: 'example',

  state: {
    title: ''
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    }
  },

  effects: {
    *fetch({ payload }, { call, put }) {  // eslint-disable-line
      yield put({
        type: 'save',
        payload: {
          title: payload.title
        },
      });
    }
  },

  reducers: {
    save(state, action) {
      return {...state, ...action.payload};
    }
  }

};
