import React from 'react';
import PropTypes from 'prop-types';
import {
  TabBar
} from 'antd-mobile';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';

import styles from './footer.less';

class Footer extends React.Component{
  aaa = () =>{
    this.props.dispatch(routerRedux.push('/page01'));
    this.props.dispatch({
      type: 'example/fetch',
      payload: {
        title: '消息'
      }
    })
  }
  render() {
    return (
      <div className={styles.normal}>
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#33A3F4"
          barTintColor="white"
          hidden={false}
        >
          <TabBar.Item
            title="主页"
            key="主页"
            icon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/sifuoDUQdAFKAVcFGROC.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selectedIcon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/iSrlOTqrKddqbOmlvUfq.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selected={this.props.location.pathname === '/'}
            badge={1}
            onPress={() => {
              this.props.dispatch(routerRedux.push('/'));
              this.props.dispatch({
                type: 'example/fetch',
                payload: {
                  title: '主页'
                }
              })
            }}
            data-seed="shenghuo"
          >
            {this.props.childrens}
          </TabBar.Item>
          <TabBar.Item
            title="消息"
            key="消息"
            icon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://gw.alipayobjects.com/zos/rmsportal/BTSsmHkPsQSPTktcXyTV.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selectedIcon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://gw.alipayobjects.com/zos/rmsportal/ekLecvKBnRazVLXbWOnE.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selected={this.props.location.pathname === '/page01'}
            onPress={this.aaa.bind(this)}
            data-seed="koubei"
          >
            {this.props.childrens}
          </TabBar.Item>
          <TabBar.Item
            title="应用中心"
            key="应用中心"
            icon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/psUFoAMjkCcjqtUCNPxB.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selectedIcon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/IIRLrXXrFAhXVdhMWgUI.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selected={this.props.location.pathname === '/page02'}
            onPress={() => {
              this.props.dispatch(routerRedux.push('/page02'))
              this.props.dispatch({
                type: 'example/fetch',
                payload: {
                  title: '应用中心'
                }
              })
            }}
            dot
            data-seed="pengyou"
          >
            {this.props.childrens}
          </TabBar.Item>
          <TabBar.Item
            title="我的"
            key="我的"
            icon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/asJMfBrNqpMMlVpeInPQ.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selectedIcon={
              <div style={{
                width: '22px',
                height: '22px',
                background: 'url(https://zos.alipayobjects.com/rmsportal/gjpzzcrPMkhfEqgbYvmN.svg) center center /  21px 21px no-repeat' }}
              />
            }
            selected={this.props.location.pathname === '/page03'}
            onPress={() => {this.props.dispatch(routerRedux.push('/page03'))
              this.props.dispatch({
                type: 'example/fetch',
                payload: {
                  title: '我的'
                }
              })
            }}
            data-seed="wode"
          >
            {this.props.childrens}
          </TabBar.Item>
        </TabBar>
      </div>
    )
  }
}


// Footer.propTypes = {
//   dispatch: PropTypes.func.isRequired,
//   childrens: PropTypes.element.isRequired,
//   location: PropTypes.object.isRequired
// };



export default connect()(Footer);
