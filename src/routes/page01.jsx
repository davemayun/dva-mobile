import React from 'react';
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './page01.less';

import Main from '../layouts/main.jsx';
import xx1 from '../assets/xx/xx1.png'

function Page01({
  dispatch,
  location
}) {
  return (
    <Main location={location}>
      <div className={styles.normal}>
        <img onClick={() =>{
          dispatch(routerRedux.push('/xx_page1'));
        }} style={{width: '100%'}} src={xx1} alt=""/>
      </div>
    </Main>
  );
}

Page01.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Page01);
