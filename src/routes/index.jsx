import React from 'react';
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';

import styles from './index.less';
import logo1 from '../assets/index/4.png'
import logo2 from '../assets/index/3.png'
import logo3 from '../assets/index/xse.png'
import logo4 from '../assets/index/5.png'

import Main from '../layouts/main.jsx';

function Index({
  dispatch,
  location
}) {
  return (
    <Main location={location}>
      <div className={styles.normal}>
        <img onClick={() =>{
          dispatch(routerRedux.push('/xx_page1'))
        }} style={{width: '100%'}} src={logo1} alt=""/>
        <img onClick={() =>{
          dispatch(routerRedux.push('/gd'))
        }} style={{width: '100%'}} src={logo2} alt=""/>
        <img onClick={() =>{
          dispatch(routerRedux.push('/xxe'))
          dispatch({
            type: 'example/fetch',
            payload: {
              title: '销售额分析'
            }
          })
        }} style={{width: '100%'}} src={logo3} alt=""/>
        <img onClick={() =>{
          dispatch(routerRedux.push('/xxe_page1'))
        }} style={{width: '100%'}} src={logo4} alt=""/>
      </div>
    </Main>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
