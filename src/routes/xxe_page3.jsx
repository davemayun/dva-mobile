import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import xxe_page3 from '../assets/xxe/xxe-page3.png'
import gd_cjleft from '../assets/gd/gd-cjleft.png'


function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/xxe'));
      }}  style={{position: 'absolute',top: '0',left: '0',width:'13%'}} src={gd_cjleft} alt=""/>
      <img style={{width: '100%'}} src={xxe_page3} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
