import React from 'react';
import PropTypes from 'prop-types';
import {
  NavBar, Icon
} from 'antd-mobile';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './header.less';
import fqlt from '../assets/xx/fqlt.png'

class Header extends React.Component {
  state = {
    isTrue: false
  }
  componentDidMount() {
  }
  render() {
    return (
      <div className={styles.normal}>
        <NavBar
          mode="light"
          icon={<Icon type="left" />}
          onLeftClick={() => {
            this.props.dispatch(routerRedux.push('/'));
            this.props.dispatch({
              type: 'example/fetch',
              payload: {
                title: '主页'
              }
            })
          }}
          rightContent={[
            <Icon key="0" type="search" style={{marginRight: '16px'}} />,
            <Icon style={{zIndex: 999}} onClick={() =>{
              if(this.props.location.pathname === '/page01') {
                let a = !this.state.isTrue;
                this.setState({
                  isTrue: a
                })
              }
            }} key="1" type="ellipsis" />
          ]}
        >{this.props.title||'主页'}</NavBar>
        {
          this.state.isTrue?<img onClick={() =>{
            this.props.dispatch(routerRedux.push('/xx_page2'));
          }} style={{zIndex:999, width:100,position: 'absolute',right:0,top: '30px'}} src={fqlt} alt=""/>:''
        }
      </div>
    );
  }
}
Header.propTypes = {
  location: PropTypes.object.isRequired
};
function mapStateToProps(state) {
  return {
    title: state.title,
    ...state.example
  };
}
export default connect(mapStateToProps)(Header);
