import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import gd_1 from '../assets/gd/gd-1.png'
import gd_2 from '../assets/gd/gd-2.png'
import gd_3 from '../assets/gd/gd-3.png'
import gd_4 from '../assets/gd/gd-4.png'
import gd_cjleft from '../assets/xxe/xxe-left.png'
import gdxq from '../assets/gd/gdxq.png'
function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd_cj'));
      }} style={{position: 'absolute',top: '1.1%',right: '0',width:'13%'}} src={gdxq} alt=""/>
      <img onClick={() =>{
        dispatch(routerRedux.push('/'));
        dispatch({
          type: 'example/fetch',
          payload: {
            title: '主页'
          }
        })
      }}  style={{position: 'absolute',top: '1%',left: '3%',width:'13%'}} src={gd_cjleft} alt=""/>
      <img style={{width: '100%',marginBottom: '-24px'}} src={gd_1} alt=""/>
      <img style={{width: '100%',marginBottom:'-5px'}} src={gd_2} alt=""/>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd_page1'));
      }} style={{width: '100%',marginBottom:'-7px'}} src={gd_3} alt=""/>
      <img style={{width: '100%'}} src={gd_4} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
