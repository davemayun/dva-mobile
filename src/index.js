import 'babel-polyfill';
import dva from 'dva';

import './index.less';
// history or browerHistory
import {createBrowserHistory} from 'history'
// 1. Initialize
const app = dva({
  history: createBrowserHistory({ basename: '/' }),
});

// 2. Plugins
// app.use();

// 3. Model
// app.model(require('./models/example'));
// 在router里面按需加载

// 4. Router
app.router(require('./router.jsx'));

// 5. Start
app.start('#root');
