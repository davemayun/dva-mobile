import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import xx_page1 from '../assets/xx/xx_page1.png'
import gd_cjleft from '../assets/gd/gd-cjleft.png'


function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/page01'));
        dispatch({
          type: 'example/fetch',
          payload: {
            title: '消息'
          }
        })
      }}  style={{position: 'absolute',top: '1%',left: '3%',width:'13%'}} src={gd_cjleft} alt=""/>
      <img style={{width: '100%'}} src={xx_page1} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
