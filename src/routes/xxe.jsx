import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import xxe_1 from '../assets/xxe/xxe-1.png'
import xxe_3 from '../assets/xxe/xxe-3.png'
import xxbb_1 from '../assets/xxe/xxbb-1.png'
import xxbb_2 from '../assets/xxe/xx-te.png'
import xxbb_3 from '../assets/xxe/xx-te2.png'
import xxbb_4 from '../assets/xxe/xxbb-4.png'
import Header from '../layouts/header'

function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <Header location={location} />
      <img style={{width: '100%'}} src={xxe_1} alt=""/>
      <Flex>
        <Flex.Item><img onClick={() =>{dispatch(routerRedux.push('/xxe_page1'));}} style={{width: '100%'}} src={xxbb_1} alt=""/></Flex.Item>
        <Flex.Item><img onClick={() =>{dispatch(routerRedux.push('/xxe_page2'));}} style={{width: '100%'}} src={xxbb_2} alt=""/></Flex.Item>
        <Flex.Item><img onClick={() =>{dispatch(routerRedux.push('/xxe_page3'));}} style={{width: '100%'}} src={xxbb_3} alt=""/></Flex.Item>
        <Flex.Item><img style={{width: '100%'}} src={xxbb_4} alt=""/></Flex.Item>
      </Flex>
      <img style={{width: '100%'}} src={xxe_3} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
