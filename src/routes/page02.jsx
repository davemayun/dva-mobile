import React from 'react';
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './page02.less';

import Main from '../layouts/main.jsx';
import yy1 from '../assets/yy/yy1.png'
import yy1_1 from '../assets/yy/yy1-1.png'
import yy1_2 from '../assets/yy/yy1-2.png'
import yy1_3 from '../assets/yy/yy1-3.png'
import yy1_4 from '../assets/yy/yy1-4.png'
import yy1_5 from '../assets/yy/yy1-5.png'
import yy1_6 from '../assets/yy/yy1-6.png'
import yy1_7 from '../assets/yy/yy1-7.png'
function Page02({
  dispatch,
  location
}) {
  return (
    <Main location={location}>
      <div className={styles.normal}>
        <img style={{width: '100%'}} src={yy1} alt=""/>
        <div>
          <img onClick={() =>{
            dispatch(routerRedux.push('/gd'))
          }} style={{width: '33.3333%',height:100}} src={yy1_1} alt=""/>
          <img style={{width: '33.3333%',height:100}} src={yy1_2} alt=""/>
          <img onClick={() =>{
            dispatch(routerRedux.push('/xxe'))
          }} style={{width: '33.3333%',height:100}} src={yy1_3} alt=""/>
        </div>
        <div>
          <img onClick={() =>{
            dispatch(routerRedux.push('/page01'))
            dispatch({
              type: 'example/fetch',
              payload: {
                title: '消息'
              }
            })
          }} style={{width: '33.3333%',height:100}} src={yy1_4} alt=""/>
          <img style={{width: '33.3333%',height:100}} src={yy1_5} alt=""/>
          <img style={{width: '33.3333%',height:100}} src={yy1_6} alt=""/>
        </div>
        <img style={{width: '100%',height: 133}} src={yy1_7} alt=""/>
      </div>
    </Main>
  );
}

Page02.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Page02);
