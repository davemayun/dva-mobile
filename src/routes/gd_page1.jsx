import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import gd_page1 from '../assets/gd/gd_page1.png'
import gd_page1_1 from '../assets/gd/gd_page1_1.png'
import gd_page1_2 from '../assets/gd/gd_page1_2.png'
import gd_page1_3 from '../assets/gd/gd_page1_3.png'
import gd_page1_4 from '../assets/gd/gd_page1_4.png'
import gd_page1_5 from '../assets/gd/gd_page1_5.png'
import gd_cjleft from '../assets/gd/gd-cjleft.png'


function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd'));
      }}  style={{position: 'absolute',top: '1%',left: '3%',width:'13%'}} src={gd_cjleft} alt=""/>
      <img style={{width: '100%'}} src={gd_page1} alt=""/>
      <img style={{width: '100%'}} src={gd_page1_1} alt=""/>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd_page2'));
      }} style={{width: '100%'}} src={gd_page1_2} alt=""/>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd_page3'));
      }} style={{width: '100%'}} src={gd_page1_3} alt=""/>
      <img style={{width: '100%'}} src={gd_page1_4} alt=""/>
      <img style={{width: '100%'}} src={gd_page1_5} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
