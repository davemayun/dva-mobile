import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import xxe_page1 from '../assets/xxe/xxe-page1.png'
import xxe_left from '../assets/xxe/xxe-left.png'


function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/xxe'));
        dispatch({
          type: 'example/fetch',
          payload: {
            title: '销售额分析'
          }
        })
      }}  style={{position: 'absolute',top: '0.5%',left: '0',width:'13%'}} src={xxe_left} alt=""/>
      <img style={{width: '100%'}} src={xxe_page1} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
