import React from 'react';
import {Flex} from 'antd-mobile'
import PropTypes from 'prop-types';
import {
  connect
} from 'dva';
import {
  routerRedux
} from 'dva/router';
import styles from './index.less';
import gd_cj from '../assets/gd/gd-cj.png'
import gd_tj from '../assets/gd/gd-tj.png'
import gd_cjleft from '../assets/gd/gd-cjleft.png'

function Index({
  dispatch,
  location
}) {
  return (
    <div className={styles.normal} style={{paddingTop:0}}>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd_page1'));
      }} style={{position: 'absolute',top: '1.4%',right: '5%',width:'13%'}} src={gd_tj} alt=""/>
      <img onClick={() =>{
        dispatch(routerRedux.push('/gd'));
      }}  style={{position: 'absolute',top: '1%',left: '3%',width:'13%'}} src={gd_cjleft} alt=""/>
      <img style={{width: '100%'}} src={gd_cj} alt=""/>
    </div>
  );
}

Index.propTypes = {
  location: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Index);
