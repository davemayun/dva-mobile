# dva-antd-mobile [![Build Status](https://travis-ci.org/xlsdg/dva-antd-mobile-starter.svg?branch=master)](https://travis-ci.org/xlsdg/dva-antd-mobile-starter)

> Get started with Dva.js and Ant Design mobile.

> this is a demo example.

> npm install or yarn.

> npm start.

![img](https://gitee.com/davemayun/dva-mobile/raw/master/sm.gif)